module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Welcome',
      items: ['welcome/modules/modules', 'welcome/paths/paths'],
    },
    {
      type: 'category',
      label: 'Blockchain Basics',
      items: [
        'blockchain-basics/introduction/introduction',
        'blockchain-basics/decentralized-storage/decentralized-storage',
        'blockchain-basics/decentralized-currency/decentralized-currency',
        'blockchain-basics/consensus-mechanism/consensus-mechanism',
        'blockchain-basics/smart-contracts/smart-contracts'
      ],
    },
    {
      type: 'category',
      label: 'Tezos Basics',
      items: [
        'tezos-basics/introduction/introduction',
        'tezos-basics/nodes/nodes',
        'tezos-basics/liquid-proof-of-stake/liquid-proof-of-stake',
        'tezos-basics/operations/operations',
        'tezos-basics/cli-and-rpc/cli-and-rpc',
        'tezos-basics/governance-on-chain/governance-on-chain',
        'tezos-basics/economics-and-rewards/economics-and-rewards',
        'tezos-basics/tezos-performances/tezos-performances',
        'tezos-basics/tickets/tickets',
        'tezos-basics/sapling/sapling',
        'tezos-basics/test_networks/test-networks'
      ],
    },
    {
      type: 'category',
      label: 'Smart contracts',
      items: [
        'smart-contracts/simple-nft-contract-1/simple-nft-contract-1',
        'smart-contracts/call-and-deploy/call-and-deploy',
        'smart-contracts/simple-nft-contract-2/simple-nft-contract-2',
        'smart-contracts/simplified-contracts/simplified-contracts',
        'smart-contracts/smart-contracts-concepts/smart-contracts-concepts',
        'smart-contracts/avoiding-flaws/avoiding-flaws',
        'smart-contracts/oracles/oracles'
      ],
    },
    {
      type: 'category',
      label: 'Node and Baking',
      items: [
        'node-baking/overview/overview',
        {
          type: 'category',
          label: 'Node',
          items: [
            'node-baking/deploy-a-node/introduction/introduction',
            'node-baking/deploy-a-node/installation/installation',
            'node-baking/deploy-a-node/upgrade/upgrade',
            'node-baking/deploy-a-node/monitor-a-node/monitor-a-node',
            'node-baking/deploy-a-node/networks/networks',
            'node-baking/deploy-a-node/best-practices/best-practices',
            'node-baking/deploy-a-node/to-go-further/to-go-further',
            'node-baking/deploy-a-node/node-cluster/node-cluster'
          ]
        },
        {
          type: 'category',
          label: 'Baking',
          items: [
            'node-baking/baking/introduction/introduction',
            'node-baking/baking/baking_explained/baking_explained',
            'node-baking/baking/reward/reward',
            'node-baking/baking/risks/baking_risks',
          ]
        }
      ]
    },
    {
      type: 'category',
      label: 'How to use an Explorer',
      items: [
        'explorer/introduction/introduction',
        'explorer/indexer-explained/indexer-explained',
        'explorer/available-tezos-indexers/available-tezos-indexers',
        'explorer/tzstats-main-features/tzstats-main-features',
        'explorer/tzstats-smart-contract/tzstats-smart-contract',
        'explorer/private-indexer/private-indexer'
      ],
    },
    {
      type: 'category',
      label: 'Archetype',
      items: [
        'archetype/introduction/introduction',
        'archetype/completium/completium',
        {
          type: 'category',
          label: 'Raffle example',
          items: ['archetype/raffle-example/contract/raffle-contract', 'archetype/raffle-example/test/unit-test'],
        },
        {
          type: 'category',
          label: 'Events',
          items: ['archetype/events/intro/emit-receive', 'archetype/events/example/event-example', 'archetype/events/dappexample/dapp-event-example'],
        },
        'archetype/templates/examples'
      ],
    },
    {
      type: 'category',
      label: 'SmartPy',
      items: ['smartpy/introduction/introduction', 'smartpy/installation/installation', 'smartpy/write-contract-smartpy/write-contract-smartpy'],
    },
    {
      type: 'category',
      label: 'LIGO',
      items: [
        'ligo/introduction/introduction',
        'ligo/installation/installation',
        {
          type: 'category',
          label: 'Raffle example',
          items: ['ligo/write-contract-ligo/raffle-contract/1-raffle-contract', 'ligo/write-contract-ligo/launch-raffle/2-launch-raffle', 'ligo/write-contract-ligo/buy-ticket/3-buy-ticket', 'ligo/write-contract-ligo/close-raffle/4-close-raffle', 'ligo/write-contract-ligo/refactoring/5-refactoring'],
        },
        'ligo/deploy-a-contract/deploy-a-contract',
        'ligo/unit-testing/unit-testing',
        'ligo/examples/examples',
        'ligo/take-away/take-away',
      ],
    },
    {
      type: 'category',
      label: 'Michelson',
      items: [
        'michelson/introduction/introduction',
        'michelson/smart-contracts/smart-contracts',
        'michelson/tutorial/tutorial',
        'michelson/examples/examples',
        'michelson/instructions-reference/instructions-reference',
        'michelson/take-away/take-away',
      ],
    },
    {
      type: 'category',
      label: 'Build a Dapp',
      items: [
        'dapp/introduction/introduction',
        'dapp/taquito/taquito',
        'dapp/temple-wallet/temple-wallet',
        {
          type: 'category',
          label: 'Deploy',
          items: [
            'dapp/deploy/introduction/introduction',
            'dapp/deploy/deploy-with-taquito/deploy-with-taquito',
          ]
        },
        'dapp/frontend-basics/frontend-basics',
        'dapp/frontend-advanced/frontend-advanced',
        {
          type: 'category',
          label: 'Using indexers',
          items: [
            'dapp/indexers/introduction/introduction',
            'dapp/indexers/how_indexers_work/how_indexers_work',
            'dapp/indexers/full_and_selective_indexers/full_and_selective_indexers',
            'dapp/indexers/tzkt/tzkt',
            'dapp/indexers/que_pasa/que_pasa',
            'dapp/indexers/dip_dup_dappetizer/dip_dup_dappetizer'
          ]
        }
      ],
    },
    {
      type: 'category',
      label: 'DeFi',
      items: [
        'defi/introduction/introduction',
        'defi/token-standards/token-standards',
        'defi/dexs/dexs',
        'defi/wrapped-assets/wrapped-assets',
        'defi/cross-chain-swaps/cross-chain-swaps',
        'defi/oracles/oracles',
        'defi/stablecoins/stablecoins',
        'defi/synthetics/synthetics',
        'defi/dao/dao',
        'defi/lending/lending',
        'defi/decentralized-fundraising/decentralized-fundraising',
        {
          type: 'category',
          label: 'Rolling SAFE',
          items: ['defi/rolling-safe/presentation/presentation', 'defi/rolling-safe/smart-contract/smart-contract'],
        }
      ],
    },
    {
      type: 'category',
      label: 'Gaming',
      items: [
        {
          type: 'category',
          label: 'Unity-SDK',
          items: [
            'gaming/unity-sdk/introduction/introduction',
            'gaming/unity-sdk/quickstart/quickstart',
            'gaming/unity-sdk/connecting-accounts/connecting-accounts',
            'gaming/unity-sdk/calling-contracts/calling-contracts',
            'gaming/unity-sdk/managing-tokens/managing-tokens',
            'gaming/unity-sdk/upgrading/upgrading',
            'gaming/unity-sdk/other-use-cases/other-use-cases',
            {
              type: 'category',
              label: 'Reference',
              link: {
                type: 'doc',
                id: 'gaming/unity-sdk/reference/reference',
              },
              items: [
                'gaming/unity-sdk/reference/API/API',
                'gaming/unity-sdk/reference/DataProviderConfigSO/DataProviderConfigSO',
                'gaming/unity-sdk/reference/events/events',
                'gaming/unity-sdk/reference/TezosConfigSO/TezosConfigSO',
              ]
            },
          ]
        }
      ],
    },
    {
      type: 'category',
      label: 'Self sovereign identity',
      items: [
        'self-sovereign-identity/introduction/introduction',
        'self-sovereign-identity/challenges/challenges',
        'self-sovereign-identity/development/development',
        'self-sovereign-identity/did/did',
        'self-sovereign-identity/did-platforms/did-platforms',
        'self-sovereign-identity/tezos-did/tezos-did',
        'self-sovereign-identity/sources/sources',
      ],
    },
    {
      type: 'category',
      label: 'Formal Verification',
      items: [
        'formal-verification/introduction/introduction',
        'formal-verification/general/general',
        'formal-verification/coq/coq',
        'formal-verification/michocoq/michocoq',
        'formal-verification/modeling-theorem/modeling-theorem'
      ],
    },
    {
      type: 'category',
      label: 'Private Blockchain',
      items: [
        'private/introduction/introduction',
        'private/installation/installation',
        'private/genesis/genesis',
        'private/starting-blockchain/starting-blockchain',
        'private/using-blockchain/using-blockchain',
        'private/vpn/vpn'
      ],
    },
    {
      type: 'category',
      label: 'How to contribute',
      items: [
        'contribute/introduction/introduction',
        'contribute/report-issue/report-issue',
        'contribute/opentezos/opentezos',
        'contribute/tezos-core/tezos-core',
        'contribute/grant/grant',
        'contribute/baker/baker'
      ],
    }
  ],
}
