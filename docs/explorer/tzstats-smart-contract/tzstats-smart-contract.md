---
id: tzstats-smart-contract
title: Checkout your smart contract on TzStats
authors: Benjamin Pila
---

As a developer, you will often want to check the state of your deployed smart contracts. Using a blockchain explorer is a fast and easy way to do so. In this section, we will find a smart contract and check it out on _TzStats_.

## Find your smart contract on _TzStats_

Once you have the address of your smart contract, go to the _TzStats_ website associated with the network you deployed your contract on.

Remember, the following TzStats website links are:

- **Mainnet**: [tzstats.com](https://tzstats.com)
- **Ghostnet**: [ghost.tzstats.com](https://ghost.tzstats.com)

Copy/paste your address in the search bar:

![TzStats search bar](tzStats_search_bar.png)

TzStats then shows a page with information related to your smart contract, e.g. the balance of the contract, the amounts sent and received, the creator address, etc.

![TzStats webpage of a smart contract](tzStats_smart_contract_general_information.png)

Below the general information, you have a list of tabs allowing you to see:

- the calls
- the entry points
- the contract code
- the storage
- the different _big maps_ of your smart contract (if there are any)

For the following examples, we will check out the smart contract, deployed on the Mainnet, whose address is the following:

```
KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn
```

Available at this [link](https://tzstats.com/KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn).

### Calls

Here you have the history of all transactions related to your smart contract.

It appears:

- The transactions with the amount and the address of the sender.
- The calls made to the entrypoints with the name of the entrypoint in question.
- On the right the status of the block with the number of confirmation received.

![TzStats webpage with a list of smart contract calls](tzStats_smart_contract_calls.png)

### Entrypoints

Here you have a list of all your entrypoints and their parameters. Furthermore, you can see how many calls each entrypoint has received.

![TzStats webpage with the list of a smart contract entrypoints](tzStats_smart_contract_entrypoints.png)

### Contract Code

Here you have the Michelson code of your smart contract.

![TzStats webpage of the Michelson code of a smart contract](tzStats_smart_contract_code.png)

### Storage

Here you have access to the content of your storage with the type of each variable and its current value. Notice that the content excludes big maps as they are specific tabs for them.

![TzStats webpage of the storage of a smart contract](tzStats_smart_contract_storage.png)

### Big map

Here you have the content of your bigmap:

![TzStats webpage of a big map](tzStats_smart_contract_bigmap.png)

## API Calls

The same pieces of information can be retrieved by API calls, without using the frontend.
A full documentation is available [here](https://docs.tzpro.io/).

# Conclusion

tzstats.com is extremely useful to monitor what is going-on on the mainnet and public testnets.
All the pieces of information regarding the cycles, the blocks, the transactions, the smarts contracts... can quickly be found,
thanks to a user-friendly interface.

In addition, _TzStats_ provides a complete and free REST API (for non-commercial use), without any authentication or enforcement of rate limits (as long as it remains reasonable). See the introduction of the [Tzstats API](https://tzstats.com/docs/api#tezos-api).

Those calls can be performed by any library: the pieces of information retrieved about a public Tezos network can be used in another monitoring tool, or even in Dapps.

Indeed, API calls can be used within Dapps to retrieve those kinds of information. For instance, _taquito_ (a typescript library to interact with a tezos node) is not able to retrieve the keys of a big map with a simple call. A call to the _TzStats_ API solves this issue.

Those tools are also available for private networks.
This will be detailed in the next chapter, where a private _TzStats_ is set up to monitor a private network.

## References

[1] <https://tzstats.com/>

[2] <https://tzstats.com/KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn>

[3] <https://tzstats.com/docs/api#tezos-api>

[4] <https://tzstats.com/docs/api#explorer-endpoints>
