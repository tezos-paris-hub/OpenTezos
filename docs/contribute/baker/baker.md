---
id: baker
title: Become a baker or a delegator
authors: Aymeric Bethencourt, Mathias Hiron
---

Becoming a baker on Tezos is a great way to contribute to the ecosystem while earning some profits. _Baking_ is the process of forming new blocks on Tezos. This process is part of the Proof-of-Stake consensus (more on that in the [Tezos basics module](/tezos-basics)). The more bakers, the more decentralized and statistically safe this process is.

## How to bake?

Everyone with at least 6,000 tez can produce, sign and validate blocks and get rewards in proportion to their stake. The only thing you need to exercise your baking rights is to run a node with baking software and keep it online and up to date.

OpenTezos has a dedicated module on [How baking works](/node-baking/baking/baking_explained). However, this might be intimidating for inexperienced users — a more accessible alternative is to delegate or stake instead of baking.

## Delegation and staking

Being a baker gives the highest rewards, but requires allocating at least 6,000 tez, and having the technical ability and time to set up a baker node and run the baking software reliably with as little downtime as possible.

An easier way for you to obtain baking rewards is to become a *delegator* thus delegating all your tez, and optionally staking part of your tez, to the baker of your choice. In doing so, your receive baking rewards while avoiding the constraints of becoming a baker yourself, in return for letting the baker keep a share of the associated rewards.

### Delegation

Delegated tez are not frozen, and are at no risk of being slashed. You can spend them at any time and without any delay. Just keep in mind, you only delegate your rights; that's it.

The baking rewards are credited to the baker. The baker then manually (or using automated tools) pays delegators (people who delegated to him) their share of baking rewards after charging some service fee.

### Staking

With staking, the rewards, minus the fees kept by the baker, are directly credited to you.

The rewards are twice as high as with delegating. However, staked funds are frozen while you are staking, and there is a small risk of some of these funds being slashed if the baker you selected behaves badly.

## How to delegate or stake?

The first step is the same in both cases, as you have to first delegate to a baker, before you can then stake some tez to this baker.

Both can be done on [stake.tezos.com](https://stake.tezos.com), where you can follow the instructions.

The only thing you should worry about is choosing an excellent and reliable Tezos baker or delegation service.

[TzKT](https://tzkt.io/bakers) allows you to browse through bakers. There are a few factors to consider when choosing a baker to delegate with:

- *Fees*: How much of the rewards is the baker keeping? (often less for staking than for delegating)

- *Capacity*: Each baker has a capacity of how many coins it can accept, which is based on how many coins it currently holds itself. A baker is "over-delegated" when it has exceeded the amount of delegation it can take considering the coins they currently hold.

- *Reliability + Responsiveness*: Does this baker pay correctly and on time (when delegating)? Will this baker respond to my questions about their services? Many bakers operate forums and chat rooms in which they engage with delegators.

- *Security*: Is this baker's staking setup secure? Does this baker have a track record? Has this baker double-baked in the past and lost coins?

## How much can I earn by baking, delegating or staking?

To understand more about rewards, check our [Rewards](/node-baking/baking/reward/) section.

## References

[1] https://baking-bad.org/docs/where-to-stake-tezos/
