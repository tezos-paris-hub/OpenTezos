---
id: introduction
title: What is baking ?
authors: Aurelien - Mathias
---

In this module, we will see how baking works for the Tezos blockchain. More precisely, we will see the different actors involved, from creating new blocks to their validation. We will then see the associated reward system, and how to deploy your own baker. Finally, we will present a list of existing bakers and the criteria to evaluate them.

## What is baking?

To achieve its consensus, Tezos uses [Liquid Proof of Stake (LPoS)](/tezos-basics/liquid-proof-of-stake). That is to say that the **bakers** (also called validators) of the network temporarily lock a part of their tokens (which they cannot use anymore) to obtain the right to create and validate blocks. The creator of the next block is called the baker of that block and is chosen randomly among all the bakers, based on the number of tokens locked. In exchange for his work, the baker receives a **reward** in tez.

### What are delegating and staking?

If a tez holder does not have 6,000ꜩ or does not want to set up a computing infrastructure to bake blocks, they may stake or delegate their coins to a baker. **Staking** lets coin holders "freeze" their coins to a baker and **Delegating** lets coin holders (i.e. **delegators**) "lend" their coins to a baker. Both give the baker a higher probability of being selected to bake and attest blocks (but staking increases chances twice more than delegating). In turn, bakers share the additional revenue generated from the staked and delegated tokens with the delegators and stakers, in proportion to their participation. Note that none of these actions transfers ownership of coins. Hence bakers cannot spend or control the tez staked/delegated to them.

With _LPoS_ the number of bakers is unlimited (everyone can participate), and delegation is optional.


Baking and attestation rights are attributed at random, several cycles in advance. To deter dishonest
behavior (e.g. double baking or double attestation), **each baker must place a fixed quantity of tez
as a security deposit for a limited time**, which will be confiscated if they attempt to
compromise the chain.


The more tez a baker holds in his staking balance, the higher his chances to bake blocks and earn baking rewards.

### How do I become a baker?

For instructions on becoming a baker, see [Run a Tezos node in 5 steps](https://docs.tezos.com/tutorials/join-dal-baker) on docs.tezos.com.
