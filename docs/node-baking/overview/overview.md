---
id: overview
title: Overview
authors: Mathias Hiron et Aurélien Monteillet
---

# Overview

## Introduction to Tezos: Nodes and Baking

This page delves into two fundamental components of Tezos: nodes and baking.

This page gives an overview of what a node is and how you can deploy one yourself, then introduces baking and how you can become a baker.

It links to the different sections of the [node](/node-baking/deploy-a-node/introduction/) and [baking](/node-baking/baking/introduction/) modules, where you can find detailed information and tutorials about each topic.

### Nodes

Nodes serve as the vital organs of the Tezos network, facilitating communication, synchronization, and data storage. Each node represents a machine that maintains a copy of the blockchain's current state and updates it based on transactions sent by users. These transactions are propagated through the network and subsequently added to the chain.

#### How Nodes work

- **Communication:** Nodes constantly exchange information to ensure that all participants have a consistent view of the blockchain.
- **Validation:** Before accepting new transactions, nodes check their validity, thereby ensuring the security and integrity of the network.

Anyone can run a node and join the Tezos network. Nodes can keep different amounts of the blockchain history, depending on their configuration:

- **Rolling mode:** The most lightweight mode. It stores recent blocks with their current context.
- **Full mode (default mode):** It also stores the content of every block since genesis so that it can handle requests about them, or even recompute the ledger state of every block of the chain.
- **Archive mode:** Also stores the history of the context, allowing services like indexers to enquire about balances or staking rights from any past block.

#### Deploying your own node

If you would like to contribute to the Tezos network with your own node, you will need to:

1. [Install and setup a node](/node-baking/deploy-a-node/installation/) on your own machine.
2. [Keep track of protocol and octez upgrades](/node-baking/deploy-a-node/upgrade/) and keep your node in optimal working condition.
3. [Monitor your node's performance and activity](/node-baking/deploy-a-node/monitor-a-node/), ensuring it operates smoothly and contributes effectively to the network.
4. [Connect your node to the network of your choice](/node-baking/deploy-a-node/networks/) (mainnet, ghostnet, etc.).
5. [Adhere to the best practices for maintaining and operating a node](/node-baking/deploy-a-node/best-practices), enhancing its performance and your overall experience.

Finally, for heavy use cases, you may choose to [deploy a cluster of nodes](/node-baking/deploy-a-node/node-cluster/), streamlining the process and ensuring a robust, well-organized node infrastructure.

### Baking

Baking is the mechanism through which new blocks are forged and added to the Tezos blockchain. It implies running a dedicated piece of software that interacts with the network through an associated node. Bakers, also known as validators, get a chance to be given the opportunity to create blocks, depending on how many tokens they put at stake, following a Proof of Stake mechanism.

In return, they receive rewards in the form of transaction fees and newly minted tez for that block.

#### Key aspects of baking

- **Baking Rights:** Baking rights are assigned randomly, in proportion to the staking balance of the baker, consisting of its own tez, and tez delegated to it by other accounts. The higher this balance is, the higher the chance of the baker obtaining the right to create a block.
- **Attestations:** Once a baker proposes a new block, other bakers take part in a process involving propagating pre-attestations and attestations, so that this block is permanently added to the blockchain (what we call finality).
- **Rewards:** Bakers and delegators earn tez [rewards](/node-baking/baking/reward/) for their active participation in securing and operating the network.
- **Penalties/Risks:** Baking comes with some [risks:](/node-baking/baking/risks/) bakers who act maliciously or negligently by proposing multiple blocks at once (double baking) are punished by slashing: they lose part of their staked tez.

To learn more about the basics of baking, check out [What is baking ?](/node-baking/baking/introduction/), and [How baking works?](/node-baking/baking/baking_explained/) sections

#### Delegating

For those who either lack 6,000 tez (plus fees) or don’t have the resources or willingness to setup infrastructure, there's the option to delegate their tokens to a baker, to augment its baking rights. In exchange, delegators receive a portion of the rewards that bakers pledge to distribute. Delegated tez are neither frozen nor at risk of being slashed.

### Interaction between Nodes and Baking

A baker connects to the network through a node. The node supplies pending operations, which the baker then evaluates for validity. The baker selects a subset of these operations to form a block that it then propagates through the entire network.

It's essential to recognize that, like any software based on a peer-to-peer network, Tezos nodes are susceptible to attacks, especially for bakers. Therefore, it's crucial for bakers to protect communication channels and store private keys in a cold wallet to ensure the utmost security.

### Why deploy a node or become a baker?

[Deploying a node allows you to:](/node-baking/deploy-a-node/installation/)

- Interact with the blockchain without relying on a public node hosted by a third party.
- Contribute to the decentralization of the blockchain, therefore its resilience and security.

[Becoming a baker allows you to:](/node-baking/baking/introduction/)

- Be rewarded for baking new blocks and attesting other bakers’ blocks.
- Increase the security of the blockchain by helping to spread the responsibility and power associated with creating new blocks.
- Ensure your ability to add your transactions to the blockchain, at a predictable rate and cost.
