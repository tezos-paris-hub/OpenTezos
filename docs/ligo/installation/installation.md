---
id: installation
title: Installation
authors: Maxime Sallerin and Benjamin Pilia
---

There are currently four ways to get started with LIGO. You can choose to use a Docker image, a static Linux binary, install packages for your Debian Linux distribution, or you can try directly on the [online editor](https://ide.ligolang.org/).

## Ligo installation

[Official Ligo installation page](https://ligolang.org/docs/intro/installation?lang=jsligo)

## Online Editor

The online editor is the quickest and easiest way to get started.

You can choose your preferred syntax and write your LIGO instructions directly in the text editor. It is available at [ligolang.org](https://ligolang.org/) in the [Try Online](https://ide.ligolang.org/) tab.

![Ligo online IDE webpage](screenshot_online_editor.png)
<small className="figure">FIGURE 1: Try Online</small>

You can then execute your code by choosing from the following features:

- Compile
- Compile Function
- Deploy
- Dry Run
- Evaluate Function
- Evaluate Value
- Generate Deploy Script

![Drop-down menu: Compile, Compile Function, Deploy, Dry Run, Evaluate Function, Evaluate Value, Generate Deploy Script](screenshot_online_editor_compilation.png)
<small className="figure">FIGURE 2: Online Editor Compilation </small>

# Editor Support

Painters need a brush and a canvas. Developers need a good IDE experience. LIGO currently offers support for [VSCode](https://code.visualstudio.com), including syntax highlighting and on-the-fly compilation error reporting.

Available extensions:

- **[LIGO VSCode Plugin](https://marketplace.visualstudio.com/items?itemName=ligolang-publish.ligo-vscode)**
- **[On-the-fly compilation error reporting](https://marketplace.visualstudio.com/items?itemName=Ligo.ligo-tools)**

# References

[1] https://ligolang.org/docs/intro/installation

[2] https://ide.ligolang.org/
